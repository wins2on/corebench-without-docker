apt-get install \
        --yes       \
        --no-install-recommends \
        --no-install-suggests \
    autoconf        \
    autogen         \
    autopoint       \
    automake        \
    bison           \
    clang           \
    cvs             \
    gettext         \
    gcc             \
    git             \
    gnuplot         \
    gperf           \
    gzip            \
    lcov            \
    libtool         \
    make            \
    nasm            \
    patch           \
    perl            \
    rsync           \
    tar             \
    texinfo         \
    subversion      \
    unzip           \
    vim             \
    wget

apt-get install \
        --yes       \
        --no-install-recommends \
        --no-install-suggests \
    supervisor      \
    python-dev build-essential \
    openssh-server sudo net-tools \
    lxde-core lxde-icon-theme x11vnc xvfb screen openbox nodejs firefox \
    htop bmon nano lxterminal


apt-get clean
rm -rf /var/lib/apt/lists/*

#FIX problem with aclocal and pkg-config
wget http://pkgconfig.freedesktop.org/releases/pkg-config-0.28.tar.gz
tar -zxvf pkg-config-0.28.tar.gz
cd pkg-config-0.28
./configure --with-internal-glib
make
make install
#FIX problem with aclocal
cp /usr/local/share/aclocal/* /usr/share/aclocal
mv /usr/local/share/aclocal /tmp
cd ..

locale-gen en_US.UTF-8
locale-gen ru_RU.KOI8-R
locale-gen tr_TR.UTF-8
locale-gen ja_JP.UTF-8
locale-gen en_HK.UTF-8
locale-gen zh_CN
localedef -i ja_JP -c -f SHIFT_JIS /usr/lib/locale/ja_JP.sjis

tar -zxvf corebench.tar.gz
mkdir corerepo

cd corebench

# Install make, grep, find and coreutils
./createCoREBench.sh compile-all make $HOME/corebench-without-docker/corerepo
./createCoREBench.sh compile-all grep $HOME/corebench-without-docker/corerepo
./createCoREBench.sh compile-all find $HOME/corebench-without-docker/corerepo
./createCoREBench.sh compile-all core $HOME/corebench-without-docker/corerepo


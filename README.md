# CoREBench without Docker
Copy of the actual Regression Errors for Software Debugging and Repair Research, available at http://www.comp.nus.edu.sg/~release/corebench/. Current copy allows to run test cases without creating Docker image.

The execution file
* resolves all dependencies of coreutils, findutils, grep, and make,
* installs the revisions of 70 regression errors,
  * after the bug was introduced, and
  * after the bug was fixed


Test cases for each regression error
* before the bug was introduced (should pass),
* after the bug was introduced (should fail),
* before the bug was fixed (should fail), and
* after the bug was fixed (should pass)

http://www.gnu.org/software/make/ <br />
http://www.gnu.org/software/grep/ <br />
http://www.gnu.org/software/findutils/ <br />
http://www.gnu.org/software/coreutils/

## Requirements
Installation requires Ubuntu 14.04 (will not work with newer versions) and more than 10GBs of free disk space.

## Installation
Clone the current repository into `/home/` and then run (with root privileges):
```
chmod +x execute.sh
sudo ./execute.sh
```

## Running tests
After installation,
```
cd corebench/
./executeTests.sh test-all make $HOME/corebench-without-docker/corerepo
./executeTests.sh test-all grep $HOME/corebench-without-docker/corerepo
./executeTests.sh test-all find $HOME/corebench-without-docker/corerepo
./executeTests.sh test-all core $HOME/corebench-without-docker/corerepo
```
